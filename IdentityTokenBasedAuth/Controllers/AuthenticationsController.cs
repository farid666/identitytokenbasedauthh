﻿using IdentityTokenBasedAuth.Domain.Response;
using IdentityTokenBasedAuth.Domain.Services;
using IdentityTokenBasedAuth.Resource;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IdentityTokenBasedAuth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationsController : ControllerBase
    {

        private IAuthenticationService _authenticationService;

        public AuthenticationsController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpGet(Name = "IsAuthenticated")]
        public IActionResult IsAuthenticated()
        {
            return Ok(User.Identity.IsAuthenticated);
        }

        [HttpPost(Name = "SignUp")]
        public async Task<IActionResult> Register(UserResource register)
        {
            BaseResponse<UserResource> response =  await _authenticationService.Register(register);

            if (response.Success)
            {
                return Ok(response.Extra);
            }
            else
            {
                return BadRequest(response.Message);
            }
        }

        [HttpPost(Name = "SignIn")]
        public async Task<IActionResult> Login(LoginReource login)
        {
            var response = await _authenticationService.Login(login);

            if (response.Success)
            {
                return Ok(response.Extra);
            }
            else
            {
                return BadRequest(response.Message);
            }
        }

        [HttpPost("CreateTokenByRefreshToken")]
        public async Task<IActionResult> CreateAccessTokenByRefreshToken(RefreshTokenResource refreshTokenResource)
        {
            var response = await _authenticationService.CreateAccessTokenByRefreshToken(refreshTokenResource);

            if (response.Success)
            {
                return Ok(response.Extra);
            }
            else
            {
                return BadRequest(response.Message);
            }
        }

        [HttpDelete(Name = "RevokeRefreshToken")]
        public async Task<IActionResult> RevokeRefreshToken(RefreshTokenResource refreshTokenResource)
        {
            var response = await _authenticationService.RevokeRefreshToken(refreshTokenResource);

            if (response.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest(response.Message);
            }
        }
    }
}
