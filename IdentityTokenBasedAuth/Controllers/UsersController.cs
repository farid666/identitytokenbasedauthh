﻿using IdentityTokenBasedAuth.Domain.Services;
using IdentityTokenBasedAuth.Resource;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace IdentityTokenBasedAuth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase,IActionFilter
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            var user = await _userService.GetUserByUserName(User.Identity.Name);

            return Ok(user.Adapt<UserResource>());
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // 
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            context.ModelState.Remove("Password"); // bu updateUser edende password required verir ama men passwordu edit etmek isdemirem bu metodnan thats why bu metod cagrisilamamis ise dusur
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser(UserResource user)
        {
            var response = await _userService.UpdateUser(user,User.Identity.Name);

            if (response.Success)
            {
                return Ok(response.Extra);
            }
            else
            {
                return BadRequest(response.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> UploadUserPicture(IFormFile picture)
        {
            var fileName = Guid.NewGuid().ToString()+Path.GetExtension(picture.FileName);

            var path = Path.Combine(Directory.GetCurrentDirectory()+"wwwroot/UserPictures",fileName);

            using (var stream = new FileStream(path,FileMode.Create))
            {
                await picture.CopyToAsync(stream);
            }

            var result = new
            {
                path = "https://" + Request.Host + "UserPictures" + fileName
            };

            var response = await _userService.UploadPicture(result.path,User.Identity.Name);

            if (response.Success)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(response.Message);
            }
        }
    }
}
