﻿using IdentityTokenBasedAuth.Domain.Response;
using IdentityTokenBasedAuth.Resource;
using IdentityTokenBasedAuth.Security.Token;

namespace IdentityTokenBasedAuth.Domain.Services
{
    public interface IAuthenticationService
    {
        Task<BaseResponse<UserResource>> Register(UserResource register);
        Task<BaseResponse<AccessToken>> Login(LoginReource login);
        Task<BaseResponse<AccessToken>> CreateAccessTokenByRefreshToken(RefreshTokenResource token);
        Task<BaseResponse<AccessToken>> RevokeRefreshToken(RefreshTokenResource token);

    }
}
