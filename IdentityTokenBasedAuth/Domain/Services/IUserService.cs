﻿using IdentityTokenBasedAuth.Domain.Response;
using IdentityTokenBasedAuth.Models;
using IdentityTokenBasedAuth.Resource;
using System.Security.Claims;

namespace IdentityTokenBasedAuth.Domain.Services
{
    public interface IUserService
    {
        Task<BaseResponse<UserResource>> UpdateUser(UserResource userResource, string userName);
        Task<AppUser> GetUserByUserName(string userName);
        Task<BaseResponse<AppUser>> UploadPicture(string picturePath,string UserName);
        Task<Tuple<AppUser, IList<Claim>>> GetUserByRefreshToken(string refreshToken);
        Task<bool> RevokeRefreshToken(string refreshToken);

    }
}
