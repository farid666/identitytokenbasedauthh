﻿using System.ComponentModel.DataAnnotations;

namespace IdentityTokenBasedAuth.Resource
{
    public class LoginReource
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }

    }
}
