﻿namespace IdentityTokenBasedAuth.Resource
{
    public class RefreshTokenResource
    {
        public string RefreshToken { get; set; }

    }
}
