﻿using IdentityTokenBasedAuth.Enums;
using System.ComponentModel.DataAnnotations;

namespace IdentityTokenBasedAuth.Resource
{
    public class UserResource
    {
        [Required]
        public string UserName { get; set; }
        [RegularExpression(@"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$")]
        public string PhoneNumber { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Picture { get; set; }
        public string City { get; set; }
        public Gender Gender { get; set; }

    }
}
