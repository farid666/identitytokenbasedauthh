﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace IdentityTokenBasedAuth.Security.Token
{
    public static class SignHandler
    {
        public static SecurityKey GetSecurity(string securityKey)
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));
        }
    }
}
