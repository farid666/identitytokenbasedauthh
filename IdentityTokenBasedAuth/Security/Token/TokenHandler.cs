﻿using IdentityTokenBasedAuth.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

namespace IdentityTokenBasedAuth.Security.Token
{
    public class TokenHandler : ITokenHandler
    {
        private readonly CustomTokenOptions _customTokenOptions;

        public TokenHandler(IOptions<CustomTokenOptions> tokenOptions)
        {
            _customTokenOptions = tokenOptions.Value;
        }

        public AccessToken CreateAccessToken(AppUser user)
        {
            var accessTokenExpiration = DateTime.Now.AddMinutes(_customTokenOptions.AccessTokenExpiration);

            var securityKey = SignHandler.GetSecurity(_customTokenOptions.SecurityKey);

            SigningCredentials signingCredentials = new SigningCredentials(securityKey,SecurityAlgorithms.HmacSha256Signature);


            JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(
                issuer:_customTokenOptions.Issuer,
                audience:_customTokenOptions.Audience,
                expires:accessTokenExpiration,
                notBefore:DateTime.Now,
                claims:GetClaims(user),
                signingCredentials:signingCredentials
                );


            var handler = new JwtSecurityTokenHandler();
            
            var token = handler.WriteToken(jwtSecurityToken);

            AccessToken accessToken = new AccessToken();

            accessToken.Token = token;
            accessToken.RefreshToken = CreateRefreshToken();
            accessToken.Expiration = accessTokenExpiration;

            return accessToken;
                

        }

        public void RevokeAccessToken(AppUser user)
        {
            throw new NotImplementedException();//todo
        }

        private string CreateRefreshToken()
        {
            var numberByte = new Byte[32];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(numberByte);

                return Convert.ToBase64String(numberByte);
            }
        }

        private IEnumerable<Claim> GetClaims(AppUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email,user.Email),
                new Claim(ClaimTypes.Name,user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
            };

            return claims;
        }
    }
}
