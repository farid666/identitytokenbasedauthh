﻿using IdentityTokenBasedAuth.Domain.Response;
using IdentityTokenBasedAuth.Domain.Services;
using IdentityTokenBasedAuth.Models;
using IdentityTokenBasedAuth.Resource;
using IdentityTokenBasedAuth.Security.Token;
using Mapster;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace IdentityTokenBasedAuth.Service
{
    public class AuthenticationService : BaseService, IAuthenticationService
    {

        private ITokenHandler _tokenHandler;
        private IUserService _userService;
        private CustomTokenOptions _customTokenOptions;

        public AuthenticationService(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager
            , RoleManager<AppRole> roleManager
            ,ITokenHandler tokenHandler,IUserService userService
            ,IOptions<CustomTokenOptions> options) : base(userManager, signInManager, roleManager)
        {
            _tokenHandler = tokenHandler;
            _userService = userService;
            _customTokenOptions = options.Value;
        }

        public async Task<BaseResponse<AccessToken>> CreateAccessTokenByRefreshToken(RefreshTokenResource token)
        {
            var userClaim = await _userService.GetUserByRefreshToken(token.RefreshToken);

            if (userClaim.Item1 != null)
            {
                AccessToken accessToken =  _tokenHandler.CreateAccessToken(userClaim.Item1);

                Claim refreshClaimToken = new Claim("refreshToken", accessToken.RefreshToken);
                Claim refreshTokenEndDate = new Claim("refreshTokenEndDate", DateTime.Now.AddMinutes(_customTokenOptions.RefreshTokenExpiration).ToString());

                await userManager.ReplaceClaimAsync(userClaim.Item1, userClaim.Item2[0], refreshClaimToken);
                await userManager.ReplaceClaimAsync(userClaim.Item1, userClaim.Item2[1], refreshTokenEndDate);

                return new BaseResponse<AccessToken>(accessToken);
            }
            else
            {
                return new BaseResponse<AccessToken>("Refresh token unavailable");
            }
        }

        public async Task<BaseResponse<AccessToken>> Login(LoginReource login)
        {
            var user = await userManager.FindByEmailAsync(login.Email);

            if (user != null)
            {
                var isUser = userManager.CheckPasswordAsync(user, login.Password).Result;

                if (isUser)
                {
                    AccessToken accessToken = _tokenHandler.CreateAccessToken(user);

                    Claim refreshClaimToken = new Claim("refreshToken",accessToken.RefreshToken);
                    Claim refreshTokenEndDate = new Claim("refreshTokenEndDate",DateTime.Now.AddMinutes(_customTokenOptions.RefreshTokenExpiration).ToString());


                    List<Claim> refreshClaimList = userManager.GetClaimsAsync(user).Result.Where(c => c.Type.Contains("refreshToken")).ToList();

                    if (refreshClaimList.Any())
                    {
                        await userManager.ReplaceClaimAsync(user,refreshClaimList[0],refreshClaimToken);
                        await userManager.ReplaceClaimAsync(user,refreshClaimList[1],refreshTokenEndDate);
                    }
                    else
                    {
                        await userManager.AddClaimsAsync(user,new[] {refreshClaimToken,refreshTokenEndDate});
                    }

                    return new BaseResponse<AccessToken>(accessToken);
                }


                return new BaseResponse<AccessToken>("Email or Password Invalid");
            }


            return new BaseResponse<AccessToken>("Email or Password Invalid");
        }

        public async Task<BaseResponse<UserResource>> Register(UserResource register)
        {
            AppUser user = new AppUser {UserName = register.UserName,Email = register.Email };


            IdentityResult result = await userManager.CreateAsync(user,register.Password);

            if (result.Succeeded)
            {
                return new BaseResponse<UserResource>(user.Adapt<UserResource>());
            }
            else
            {
                return new BaseResponse<UserResource>(result.Errors.First().Description);
            }
        }

        public async Task<BaseResponse<AccessToken>> RevokeRefreshToken(RefreshTokenResource token)
        {
           var result =  await _userService.RevokeRefreshToken(token.RefreshToken);

            if (result)
            {
                return new BaseResponse<AccessToken>(new AccessToken());
            }
            else
            {
                return new BaseResponse<AccessToken>("Refresh Token Unavailable");
            }
        }
    }
}
