﻿using IdentityTokenBasedAuth.Domain.Response;
using IdentityTokenBasedAuth.Domain.Services;
using IdentityTokenBasedAuth.Models;
using IdentityTokenBasedAuth.Resource;
using Mapster;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace IdentityTokenBasedAuth.Service
{
    public class UserService : BaseService, IUserService
    {
        public UserService(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, RoleManager<AppRole> roleManager) : base(userManager, signInManager, roleManager)
        {
        }

        public async Task<Tuple<AppUser, IList<Claim>>> GetUserByRefreshToken(string refreshToken) //Tuple 2 eded geriye donus etmek olur
        {
            Claim claimRefreshToken = new Claim("refreshToken", refreshToken);

            var users = await userManager.GetUsersForClaimAsync(claimRefreshToken);

            if (users.Any())
            {
                var user = users.First();

                IList<Claim> userClaims =  await userManager.GetClaimsAsync(user);

                string refrehTokenEndDate = userClaims.First(c=>c.Type == "refreshTokenEndDate").Value;

                if (DateTime.Parse(refrehTokenEndDate) > DateTime.Now)
                {
                    return new Tuple<AppUser, IList<Claim>>(user,userClaims);
                }
                else
                {
                    return new Tuple<AppUser, IList<Claim>>(null,null);
                }
            }

            return new Tuple<AppUser, IList<Claim>>(null,null);
        }

        public async Task<AppUser> GetUserByUserName(string userName)
        {
            return await userManager.FindByNameAsync(userName);
        }

        public async Task<bool> RevokeRefreshToken(string refreshToken)
        {
            var result = await GetUserByRefreshToken(refreshToken);

            if(result.Item1 == null) return false;

            IdentityResult response = await userManager.RemoveClaimsAsync(result.Item1, result.Item2);

            if (response.Succeeded)
            {
                return true;
            }

            return false;

        }

        public async Task<BaseResponse<UserResource>> UpdateUser(UserResource userResource, string userName)
        {
            var user = await userManager.FindByNameAsync(userName);

            if (userManager.Users.Count(u=>u.PhoneNumber == userResource.PhoneNumber) > 1)
            {
                return new BaseResponse<UserResource>("Number already used");
            }

            user.BirthDay = userResource.BirthDay;
            user.UserName = userName;
            user.Email = userResource.Email;
            user.City = userResource.City;
            user.Gender = (int) userResource.Gender;
            user.PhoneNumber = userResource.PhoneNumber;

            IdentityResult result = await userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return new BaseResponse<UserResource>(user.Adapt<UserResource>());//Mapster kitabxanasini isdifade etdim useri modele map ucun
            }
            else
            {
                return new BaseResponse<UserResource>(result.Errors.First().Description);
            }

        }

        public async Task<BaseResponse<AppUser>> UploadPicture(string picturePath, string UserName)
        {
            var user = await userManager.FindByNameAsync(UserName);

            user.Picture = picturePath;

            IdentityResult result =  await userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return new BaseResponse<AppUser>(user);
            }
            else
            {
                return new BaseResponse<AppUser>(result.Errors.First().Description);
            }

        }
    }
}
